
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Appearance : NSObject

@end

@interface UIFont (Appearance)

+ (UIFont *)Appeareance_latoLight:(CGFloat)size;
+ (UIFont *)Appeareance_latoBold:(CGFloat)size;
+ (UIFont *)Appeareance_latoRegular:(CGFloat)size;

@end