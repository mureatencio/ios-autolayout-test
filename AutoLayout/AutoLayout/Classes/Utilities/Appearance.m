
#import "Appearance.h"

@implementation Appearance

@end

@implementation UIFont (Appearance)

+ (UIFont *)Appeareance_latoLight:(CGFloat)size {
    return [UIFont fontWithName:@"Lato-Light" size:size];
}
+ (UIFont *)Appeareance_latoBold:(CGFloat)size {
    return [UIFont fontWithName:@"Lato-Bold" size:size];
}
+ (UIFont *)Appeareance_latoRegular:(CGFloat)size {
    return [UIFont fontWithName:@"Lato-Regular" size:size];
}

@end
