
#import <UIKit/UIKit.h>

@interface UIColor (Utilities)

+ (UIColor *) colorWithHexString:(NSString *)hex;

@end

