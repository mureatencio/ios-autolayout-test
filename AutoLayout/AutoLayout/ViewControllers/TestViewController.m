
#import "TestViewController.h"
#import "CaloriesTableViewCell.h"
#import "AchievementTableViewCell.h"
#import "WorkoutTableViewCell.h"
#import "UIColor+Utilities.h"

@interface TestViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"CaloriesTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:[CaloriesTableViewCell reuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:@"AchievementTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:[AchievementTableViewCell reuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:@"WorkoutTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:[WorkoutTableViewCell reuseIdentifier]];
    
    UIImage *image = [UIImage imageNamed: @"logoNavBar"];
    UIImageView *imageview = [[UIImageView alloc] initWithImage: image];
    self.navigationItem.titleView = imageview;
    
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"f2f2f2"];
    
    UIImage *stageImage = [UIImage imageNamed:@"iconUserNumber2"];
    UIButton *stageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    stageButton.bounds = CGRectMake( 0, 0, stageImage.size.width, stageImage.size.height );
    [stageButton setImage:stageImage forState:UIControlStateNormal];
    UIBarButtonItem *btnStage = [[UIBarButtonItem alloc] initWithCustomView:stageButton];
    
    UIImage *btImage = [UIImage imageNamed:@"bluetoothConnected"];
    UIButton *btButton = [UIButton buttonWithType:UIButtonTypeCustom];
    btButton.bounds = CGRectMake( 0, 0, btImage.size.width, btImage.size.height );
    [btButton setImage:btImage forState:UIControlStateNormal];
    UIBarButtonItem *btnBluetooth = [[UIBarButtonItem alloc] initWithCustomView:btButton];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnBluetooth, btnStage, nil]];
    
    UIImage *menuImage = [UIImage imageNamed:@"iconMenu"];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.bounds = CGRectMake( 0, 0, menuImage.size.width, menuImage.size.height );
    [menuButton setImage:menuImage forState:UIControlStateNormal];
    UIBarButtonItem *btnMenu = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;
    
    UIBarButtonItem *negativeRightSpacer = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                            target:nil action:nil];
    negativeRightSpacer.width = -5;
    
    
    UIBarButtonItem *rightSpacer = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                            target:nil action:nil];
    rightSpacer.width = 15;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeRightSpacer, btnBluetooth, rightSpacer, btnStage, nil]];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, btnMenu, nil]];
}

#pragma mark - UITableViewDataSource methods

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [UITableViewCell new];
    switch (indexPath.row) {
        case 0:{
            cell = (WorkoutTableViewCell *)
            [tableView dequeueReusableCellWithIdentifier:[WorkoutTableViewCell reuseIdentifier] forIndexPath:indexPath];
        }
            break;
            
        case 1:{
            cell = (CaloriesTableViewCell *)
            [tableView dequeueReusableCellWithIdentifier:[CaloriesTableViewCell reuseIdentifier] forIndexPath:indexPath];
        }
            break;
        default:{
            cell = (AchievementTableViewCell *)
            [tableView dequeueReusableCellWithIdentifier:[AchievementTableViewCell reuseIdentifier] forIndexPath:indexPath];
        }
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat tableViewHeight = (self.tableView.frame.size.height-18);
    
    return tableViewHeight/3;
}



@end
