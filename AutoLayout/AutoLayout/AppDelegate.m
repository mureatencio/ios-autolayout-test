
#import "AppDelegate.h"
#import "TestViewController.h"

@interface AppDelegate ()
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UINavigationController *navController = [UINavigationController new];
    TestViewController *testVC = [TestViewController new];
    navController.viewControllers = @[testVC];
    self.window.rootViewController = navController;
    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];

    
    [self.window makeKeyAndVisible];
    return YES;
}

@end
