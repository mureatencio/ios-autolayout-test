
#import "AchievementTableViewCell.h"
#import "Appearance.h"

@interface AchievementTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subtitleMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomAwardMargin;
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;

@end

@implementation AchievementTableViewCell

- (void)awakeFromNib {
    self.containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.containerView.layer.shadowOffset = CGSizeMake(0.0f,0.3f);
    self.containerView.layer.shadowOpacity = .9f;
    self.containerView.layer.shadowRadius = 0.4f;

    if([self isIphone4S]){
        self.topMarginConstraint.constant = 5;
        self.bottomMarginConstraint.constant = 10;
        self.subtitleMarginConstraint.constant = 0;
        self.bottomAwardMargin.constant = 10;
    }
    
    NSDictionary * lightFontAttr = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont Appeareance_latoLight:(24) ], NSFontAttributeName, nil];
    
    NSMutableAttributedString *libTitle = [[NSMutableAttributedString alloc] initWithString:@"YOUR ACHIEVMENTS" attributes:lightFontAttr];
    NSRange rangeOfTitle = NSMakeRange(4,[libTitle length]-4);
    [libTitle addAttribute: NSFontAttributeName value:[UIFont Appeareance_latoBold:(24)] range:rangeOfTitle];
    
    [self.cellTitle setAttributedText:libTitle];
    
}


+ (NSString *)reuseIdentifier{
    return @"AchievementTableViewCellIdentifier";
}

@end
