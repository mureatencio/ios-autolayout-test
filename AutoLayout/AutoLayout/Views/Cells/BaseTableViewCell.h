
#import <UIKit/UIKit.h>
#import <sys/utsname.h>

@interface BaseTableViewCell : UITableViewCell

- (BOOL) isIphone4S;

@end
