
#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface AchievementTableViewCell : BaseTableViewCell

+ (NSString *)reuseIdentifier;

@end
