
#import "CaloriesTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Appearance.h"
#import "TEAChart.h"
#import "UIColor+Utilities.h"

@interface CaloriesTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *barcharContainer;

@end

@implementation CaloriesTableViewCell

- (void)awakeFromNib {
    self.containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.containerView.layer.shadowOffset = CGSizeMake(0.0f,0.3f);
    self.containerView.layer.shadowOpacity = .9f;
    self.containerView.layer.shadowRadius = 0.4f;
    
    if([self isIphone4S]){
        self.topMarginConstraint.constant = 5;
        self.bottomMarginConstraint.constant = 5;
    }
    
    NSDictionary * lightFontAttr = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont Appeareance_latoBold:(24) ], NSFontAttributeName, nil];
    
    NSMutableAttributedString *libTitle = [[NSMutableAttributedString alloc] initWithString:@"CALORIES / WEEK" attributes:lightFontAttr];
    NSRange rangeOfTitle = NSMakeRange(9,[libTitle length]-9);
    [libTitle addAttribute: NSFontAttributeName value:[UIFont Appeareance_latoLight:(24)] range:rangeOfTitle];
    
    [self.titleLabel setAttributedText:libTitle];
    
}

- (void)layoutSubviews{
    [self setupBarChart];
}

- (void)setupBarChart{
    self.barcharContainer.backgroundColor = [UIColor clearColor];
    TEABarChart *barChart = [[TEABarChart alloc] init];
    if([self isIphone4S]){
        barChart.isIphone4s = YES;
    }
    barChart.translatesAutoresizingMaskIntoConstraints = NO;
    barChart.data = @[@680, @1280, @1490, @500, @1360, @1180];
    UIColor *customRed = [UIColor colorWithHexString:@"A90D2E"];
    UIColor *customBlue = [UIColor colorWithHexString:@"29416C"];
    barChart.barColors = @[customRed,customRed,customRed,customRed,customRed,customBlue];
    barChart.xLabels = @[@"680",@"1280",@"1490",@"500",@"1360",@"1180"];
    barChart.backgroundColor = [UIColor clearColor];
    barChart.barSpacing = 12;
    [self.barcharContainer addSubview:barChart];
    
    // Width constraint, half of parent view width
    [self.barcharContainer addConstraint:[NSLayoutConstraint constraintWithItem:barChart
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.barcharContainer
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:0]];
    
    // Height constraint, half of parent view height
    [self.barcharContainer addConstraint:[NSLayoutConstraint constraintWithItem:barChart
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.barcharContainer
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:0]];
    
    // Center horizontally
    [self.barcharContainer addConstraint:[NSLayoutConstraint constraintWithItem:barChart
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.barcharContainer
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    // Center vertically
    [self.barcharContainer addConstraint:[NSLayoutConstraint constraintWithItem:barChart
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.barcharContainer
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0.0]];
}

+ (NSString *)reuseIdentifier{
    return @"CaloriesTableViewCellIdentifier";
}

@end
