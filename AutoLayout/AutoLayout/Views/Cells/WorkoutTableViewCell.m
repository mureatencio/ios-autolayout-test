
#import "WorkoutTableViewCell.h"
#import "Appearance.h"

@interface WorkoutTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLeftMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomRightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subtitleBottomConstraint;

@end
@implementation WorkoutTableViewCell

- (void)awakeFromNib {
    self.containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.containerView.layer.shadowOffset = CGSizeMake(0.0f,0.3f);
    self.containerView.layer.shadowOpacity = .9f;
    self.containerView.layer.shadowRadius = 0.4f;

    if([self isIphone4S]){
        self.topMarginConstraint.constant = 5;
        self.bottomLeftMarginConstraint.constant = 10;
        self.bottomRightConstraint.constant = 0;
        self.subtitleBottomConstraint.constant = 0;
    }
    
    NSDictionary * lightFontAttr = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont Appeareance_latoLight:(24) ], NSFontAttributeName, nil];
    
    NSMutableAttributedString *libTitle = [[NSMutableAttributedString alloc] initWithString:@"LAST WORKOUT" attributes:lightFontAttr];
    NSRange rangeOfTitle = NSMakeRange(4,[libTitle length]-4);
    [libTitle addAttribute: NSFontAttributeName value:[UIFont Appeareance_latoBold:(24)] range:rangeOfTitle];
    
    [self.titleLabel setAttributedText:libTitle];
    
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Calories \nBurned"];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setMinimumLineHeight:15];
    [paragraphStyle setMaximumLineHeight:23];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributedString length])];
    [attributedString addAttribute: NSFontAttributeName value:[UIFont Appeareance_latoLight:(20)] range:NSMakeRange(0, [attributedString length])];
    self.detailLabel.attributedText = attributedString ;
    
    
    self.detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.detailLabel.adjustsFontSizeToFitWidth = YES;
    self.detailLabel.numberOfLines = 2;
    self.detailLabel.minimumScaleFactor = 0.5;
}


+ (NSString *)reuseIdentifier{
    return @"WorkoutTableViewCellIdentifier";
}

@end
