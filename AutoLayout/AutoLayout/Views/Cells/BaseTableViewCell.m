
#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (BOOL) isIphone4S{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    return result.height == 480;
}

@end
